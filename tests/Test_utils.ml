open Unix
open Printf

let run_test f = 
  let start_time = Unix.gettimeofday() in
  let () = f () in
  let stat = Gc.stat() in
  let control = Gc.get() in
  let duration = Unix.gettimeofday() -. start_time in
  printf "{\"duration\": %f,\n\"minor_collections\": %d,\n\"major_collections\": %d,\n\"compactions\": %d,\n\"minor_words\": %d,\n\"promoted_words\": %d,\n\"major_words\": %d,\n\"minor_heap_size\": %d}\n" duration stat.minor_collections stat.major_collections stat.compactions (int_of_float stat.minor_words) (int_of_float stat.promoted_words) (int_of_float stat.major_words) control.minor_heap_size
