FROM ocaml/opam2
RUN sudo apt-get update && sudo apt-get -y install m4
RUN opam init -a && opam update
RUN opam repository add multicore https://github.com/ocamllabs/multicore-opam.git --set-default && opam switch create 4.06.1+multicore --empty && opam switch 4.06.1+multicore && opam install ocaml-variants.4.06.1+multicore ocamlfind
RUN opam switch create 4.06.1 && opam switch 4.06.1 && opam install ocamlfind
COPY --chown=opam tests tests
COPY --chown=opam run_tests.sh .
CMD sh ./run_tests.sh
