import json
import itertools
from collections import Counter
from collections import OrderedDict

tests = []

with open('output.txt', 'r') as run_output:
    compiler_counter = Counter()
    test_runs = run_output.read().split("==")
    for test_run in [f.strip() for f in test_runs]:
        if len(test_run) > 0:
            run_obj = {}
            run_name = test_run.split("\n")[0]
            run_name_parts = run_name.split(":")
            compiler_name = run_name_parts[0]
            run_obj['compiler'] = compiler_name
            run_obj['test_number'] = compiler_counter[compiler_name]
            compiler_counter[compiler_name] += 1
            run_obj['test_name'] = run_name_parts[1].strip()
            runs_data = test_run[test_run.find("\n"):len(test_run)]
            json_data = [f.strip() for f in runs_data.strip().split("--") if len(f) > 0]
            run_obj['data'] = [json.loads(f) for f in json_data if not f.startswith("#")]
            tests.append(run_obj)

tests.sort(key = lambda x: (x['test_number'], x['compiler']))

compilers = list(OrderedDict.fromkeys([a['compiler'] for a in tests]))

with open('results.html', 'w') as f:
    f.write('<table><thead><tr><th>Test name</th>')
    for compiler in compilers:
        f.write('<th>' + compiler + '</th>')
    f.write('</tr></thead><tbody>')
    grouped_tests = itertools.groupby(tests, key=lambda x: x['test_number'])
    for _, group in grouped_tests:
        results = list(group)
        f.write("<tr><td>" + results[0]['test_name'] + "</td>")
        for result in results:
            times = [a['duration'] for a in result['data']]
            times.sort()
            f.write("<td>%.2f - %.2f</td>" % (times[0], times[len(times)-1]))
        f.write("</tr>")
    f.write('</table>')