#!/bin/sh

for compiler in 4.06.1 4.06.1+multicore; do
  rm -rf bin
  mkdir bin
  opam switch $compiler
  opam exec -- ocamlfind ocamlopt -o bin/list-alloc -I tests/ -linkpkg -package unix tests/Test_utils.ml tests/list-alloc/lists.ml
  for data_type in int float int-tuple float-tuple string record float-array; do
    for size in 1 60000 80000 150000; do
      echo ==
      echo $compiler: list-alloc $data_type $size
      # bin/list-alloc $data_type $size
      echo --
    done
    echo
  done
  opam exec -- ocamlfind ocamlopt -o bin/retain-alloc -I tests/ -linkpkg -package unix tests/Test_utils.ml tests/retain-alloc/retain.ml
  for size in 0 25 50 75 100; do
      echo ==
      echo $compiler: retain-alloc 1000 $size
      bin/retain-alloc 1000 $size
      echo --
    echo
  done
  opam exec -- ocamlfind ocamlopt -o bin/simple-alloc -I tests/ -linkpkg -package unix tests/Test_utils.ml tests/simple-alloc/simple.ml
  echo ==
  echo $compiler: simple-alloc
  bin/simple-alloc
  echo --
  echo
  opam exec -- ocamlfind ocamlopt -o bin/finalise-alloc -I tests/ -linkpkg -package unix tests/Test_utils.ml tests/finalise-alloc/finalise.ml
  for size in 0 10 25 50 75 100; do
      echo ==
      echo $compiler: finalise-alloc $size
      bin/finalise-alloc $size
      echo --
    echo
  done
done
